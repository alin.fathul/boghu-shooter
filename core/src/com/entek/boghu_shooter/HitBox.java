package com.entek.boghu_shooter;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

public class HitBox {
    private ArrayList<Rectangle> hitBoxList = new ArrayList<>();

    public void addHitBox(float x, float y, float width, float height) {
        hitBoxList.add(new Rectangle(x, y, width, height));
    }

    public ArrayList<Rectangle> getHitBoxList() {
        return hitBoxList;
    }
}
