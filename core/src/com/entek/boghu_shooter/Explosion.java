package com.entek.boghu_shooter;

public class Explosion extends Particle {
    Explosion(float duration, float x, float y, GameContainer gameContainer) {
        super(duration, x, y, gameContainer, 12, 12);
    }

    @Override
    public void update(float delta) {

    }
}
