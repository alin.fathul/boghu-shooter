package com.entek.boghu_shooter;

import com.badlogic.gdx.math.Vector2;

public class EnemyDestroyed extends Particle {
    private float gravity = -500f;
    private Vector2 velocity = new Vector2(-50, 0);

    EnemyDestroyed(float duration, float x, float y, GameContainer gameContainer) {
        super(duration, x, y, gameContainer, 32, 13);
    }

    @Override
    public void update(float delta) {
        velocity.y += gravity * delta;
        addPosition(velocity.x * delta, velocity.y * delta);
    }
}
