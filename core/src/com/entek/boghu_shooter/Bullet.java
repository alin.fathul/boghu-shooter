package com.entek.boghu_shooter;

import com.badlogic.gdx.math.Vector2;

public class Bullet extends Entity{
    private GameContainer gameContainer;
    private float damage;
    private float destroyTimer = 0;

    Bullet(float speed, float x, float y, GameContainer gameContainer, float damage) {
        super(new Vector2(x, y), new Vector2(speed, 0), 8, 4);
        this.gameContainer = gameContainer;
        this.damage = damage;
        getHitBox().addHitBox(0, 0, 8, 4);
    }

    @Override
    public void update(float delta) {
        if (getPosition().x >= 256) {
            gameContainer.removeBullet(this);
        }

        addPosition(getVelocity().x * delta, getVelocity().y * delta);
    }

    @Override
    public void collision(Entity entity) {
        gameContainer.addParticle(new Explosion(0.1f, getPosition().x + getWidth() / 2, getPosition().y + getHeight() / 2, gameContainer));
        gameContainer.removeBullet(this);
    }

    public float getDamage() {
        return damage;
    }
}
