package com.entek.boghu_shooter;

import com.badlogic.gdx.math.Vector2;

import java.util.Random;

public class Enemy extends Entity {
    private float hitPoint;
    private String texture;
    private GameContainer gameContainer;
    private Random random = new Random();

    Enemy(float x, float y, float velX, float velY, float hitPoint, GameContainer gameContainer, String texture, HitBox hitBox) {
        super(new Vector2(x, y), new Vector2(velX, velY), 32, 13);
        this.texture = texture;
        this.hitPoint = hitPoint;
        this.gameContainer = gameContainer;
        setHitBox(hitBox);
    }

    @Override
    public void update(float delta) {
        addPosition(getVelocity().x * delta, getVelocity().y * delta);

        if (hitPoint <= 0) {
            gameContainer.removeEnemy(this);

            if (random.nextInt(2) == 1) {
                gameContainer.addParticle(new EnemyDestroyed(3.0f, getPosition().x + getWidth() / 2, getPosition().y + getHeight() / 2, gameContainer));
            }
            gameContainer.addParticle(new EnemyExplosion(0.5f, getPosition().x + getWidth() / 2, getPosition().y + getHeight() / 2, gameContainer));
        }
    }

    @Override
    public void collision(Entity entity) {
        if (entity instanceof Bullet) {
            hitPoint -= ((Bullet) entity).getDamage();
        }
    }

    public String getTexture() {
        return texture;
    }
}
