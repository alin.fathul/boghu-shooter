package com.entek.boghu_shooter;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class GameInputProcessor implements InputProcessor {
    private Plane plane;
    private OrthographicCamera cam;

    GameInputProcessor(Plane plane, OrthographicCamera cam) {
        this.plane = plane;
        this.cam = cam;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        Vector3 worldCoord = cam.unproject(new Vector3(screenX, screenY, 0));
        plane.setTargetPosition(worldCoord.x, worldCoord.y);
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        Vector3 worldCoord = cam.unproject(new Vector3(screenX, screenY, 0));
        plane.setTargetPosition(worldCoord.x, worldCoord.y);
        return true;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }
}
