package com.entek.boghu_shooter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class GameContainer {
    private ArrayList<Bullet> bulletList = new ArrayList<>();
    private Queue<Bullet> bulletRemoveQueue = new LinkedList<>();

    private ArrayList<Enemy> enemyList = new ArrayList<>();
    private Queue<Enemy> enemyRemoveQueue = new LinkedList<>();

    private ArrayList<Particle> particleList = new ArrayList<>();
    private Queue<Particle> particleRemoveQueue = new LinkedList<>();

    public ArrayList<Bullet> getBulletList() {
        return bulletList;
    }
    public ArrayList<Enemy> getEnemyList() { return enemyList; }
    public ArrayList<Particle> getParticleList() { return particleList; }

    public void removeBullet(Bullet bullet) { bulletRemoveQueue.add(bullet); }
    public void removeEnemy(Enemy enemy) { enemyRemoveQueue.add(enemy); }
    public void removeParticle(Particle particle) { particleRemoveQueue.add(particle); }


    public void removeBulletfromQueue() {
        while (!bulletRemoveQueue.isEmpty()) {
            bulletList.remove(bulletRemoveQueue.remove());
        }
    }

    public void removeEnemyfromQueue() {
        while (!enemyRemoveQueue.isEmpty()) {
            enemyList.remove(enemyRemoveQueue.remove());
        }
    }

    public void removeParticlefromQueue() {
        while (!particleRemoveQueue.isEmpty()) {
            particleList.remove(particleRemoveQueue.remove());
        }
    }

    public void addBullet(Bullet bullet) {
        this.bulletList.add(bullet);
    }
    public void addEnemy(Enemy enemy) { this.enemyList.add(enemy); }
    public void addParticle(Particle particle) { this.particleList.add(particle); }
}
