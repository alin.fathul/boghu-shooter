package com.entek.boghu_shooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class Background {
    private float x1;
    private float x2;
    private float speed;
    private Texture texture;

    Background(float speed, String fileName) {
        this.speed = speed;
        this.texture = new Texture(Gdx.files.internal(fileName));
        x1 = texture.getWidth();
    }

    public void update(float delta) {
        x1 -= speed * delta;
        x2 = x1 - texture.getWidth();

        if (x1 <= 0) {
            x1 = texture.getWidth();
            x2 = 0;
        }
    }

    public void dispose() {
        texture.dispose();
    }

    public float getX1() { return x1; }
    public float getX2() { return x2; }
    public Texture getTexture() { return texture; }
}
