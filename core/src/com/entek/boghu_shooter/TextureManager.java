package com.entek.boghu_shooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;
import java.util.Scanner;

public class TextureManager {
    private HashMap<String, Texture> textureHashMap = new HashMap<>();
    private HashMap<String, Animation<TextureRegion>> animationHashMap = new HashMap<>();
    private Background[] backgroundLevel_1 = new Background[4];
    private Scanner scanner;

    TextureManager(String textureFileName, String animationFileName) {
        FileHandle file = Gdx.files.internal(textureFileName);
        this.scanner = new Scanner(file.read());

        while (scanner.hasNextLine()) {
            String[] args = scanner.nextLine().split(",");
            textureHashMap.put(args[0], new Texture(Gdx.files.internal(args[1])));
        }
        scanner.close();

        FileHandle fileAnim = Gdx.files.internal(animationFileName);
        this.scanner = new Scanner(fileAnim.read());
        while (scanner.hasNextLine()) {
            String[] args = scanner.nextLine().split(",");
            String textureName = args[0];
            float frameDuration = Float.parseFloat(args[1]);
            int width = Integer.parseInt(args[2]);
            int height = Integer.parseInt(args[3]);
            int size = Integer.parseInt(args[4]);
            animationHashMap.put(args[0], new Animation<TextureRegion>(frameDuration / size, generateTextureArray(size, textureHashMap.get(textureName), width, height)));
        }
        scanner.close();

        backgroundLevel_1[0] = new Background(11, "Sky.png");
        backgroundLevel_1[1] = new Background(14, "Sky2.png");
        backgroundLevel_1[2] = new Background(17, "Hill2.png");
        backgroundLevel_1[3] = new Background(20, "Hill1.png");
    }

    public TextureRegion[] generateTextureArray(int size, Texture texture, int width, int height) {
        TextureRegion[] t = new TextureRegion[size];
        for (int i = 0; i < size; i++) {
            t[i] = new TextureRegion(texture, i * width, 0, width, height);
        }
        return t;
    }

    public Animation<TextureRegion> getAnimation(String animation) { return animationHashMap.get(animation); }

    public Texture getTexture(String texture) { return textureHashMap.get(texture); }

    public Background[] getBackgroundLevel_1() { return backgroundLevel_1; }

    public void dispose() {
        for (Texture texture : textureHashMap.values()) {
            texture.dispose();
        }

        for (Background background : backgroundLevel_1) {
            background.dispose();
        }
    }
}
