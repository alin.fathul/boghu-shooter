package com.entek.boghu_shooter;

import com.badlogic.gdx.math.Vector2;

public class EnemyExplosion extends Particle {
    EnemyExplosion(float duration, float x, float y, GameContainer gameContainer) {
        super(duration, x, y, gameContainer, 48, 48);
    }

    @Override
    public void update(float delta) {

    }
}