package com.entek.boghu_shooter;

public class GameWorld {
    private Plane plane;
    private GameContainer gameContainer = new GameContainer();
    private CollisionController collisionController = new CollisionController(gameContainer);
    private EnemyList enemyList;
    private WaveController waveController;

    GameWorld() {
        CsvReader csvReader = new CsvReader();
        this.enemyList = csvReader.getEnemyList("enemy.csv");
        this.waveController = new WaveController(gameContainer, "wave.txt", enemyList);
        this.plane = new Plane(100.0f, 2, 200, 40, 0.1f, 48, 26, 100, 100, gameContainer,40, 9);
    }

    public void update(float delta) {
        waveController.update(delta);
        plane.update(delta);

        for (Bullet bullet : gameContainer.getBulletList()) {
            bullet.update(delta);
        }

        for (Enemy enemy : gameContainer.getEnemyList()) {
            enemy.update(delta);
        }

        for (Particle particle : gameContainer.getParticleList()) {
            particle.update(delta);
            particle.updateTimer(delta);
        }

        collisionController.update(delta);

        gameContainer.removeBulletfromQueue();
        gameContainer.removeEnemyfromQueue();
        gameContainer.removeParticlefromQueue();
    }

    public Plane getPlane() { return plane; }

    public GameContainer getGameContainer() { return gameContainer; }
}
