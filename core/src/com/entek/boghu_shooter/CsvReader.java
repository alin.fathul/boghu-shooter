package com.entek.boghu_shooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.util.Scanner;

public class CsvReader {
    private Scanner scanner;

    public EnemyList getEnemyList(String fileName) {
        EnemyList enemyList = new EnemyList();

        FileHandle file = Gdx.files.internal(fileName);
        this.scanner = new Scanner(file.read());

        while (scanner.hasNextLine()) {
            String[] args = scanner.nextLine().split(",");
            HitBox hitBox = new HitBox();

            for (int i = 0; i < Integer.parseInt(args[5]); i++) {
                String[] hitBoxArgs = scanner.nextLine().split(",");
                hitBox.addHitBox(Float.parseFloat(hitBoxArgs[0]), Float.parseFloat(hitBoxArgs[1]), Float.parseFloat(hitBoxArgs[2]), Float.parseFloat(hitBoxArgs[3]));
            }

            enemyList.add(args[0], Float.parseFloat(args[1]), Float.parseFloat(args[2]), Float.parseFloat(args[3]), args[4], hitBox);
        }

        return enemyList;
    }
}
