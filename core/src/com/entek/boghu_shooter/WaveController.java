package com.entek.boghu_shooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.util.ArrayList;
import java.util.Scanner;

public class WaveController {
    private float time = 0;
    private int wave = 0;
    private FileHandle file;
    private Scanner scanner;
    private GameContainer gameContainer;
    private ArrayList<WaveData> waveDataList = new ArrayList<>();
    private ArrayList<SpawnPattern> spawnPatternList = new ArrayList<>();
    private EnemyList enemyList;

    WaveController(GameContainer gameContainer, String fileName, EnemyList enemyList) {
        this.enemyList = enemyList;
        this.gameContainer = gameContainer;
        this.file = Gdx.files.internal(fileName);
        this.scanner = new Scanner(file.read());

        this.spawnPatternList.add(new SpawnPattern("pattern01.txt"));
        this.spawnPatternList.add(new SpawnPattern("pattern02.txt"));

        while (scanner.hasNextLine()) {
            int id = scanner.nextInt();
            float time = scanner.nextFloat();

            for (SpawnData spawnData : spawnPatternList.get(id).getSpawnDataList()) {
                waveDataList.add(new WaveData(stringToEnemy(spawnData.enemyName, spawnData.position.x, spawnData.position.y), time + spawnData.time));
            }
        }
    }

    public void update(float delta) {
        time += delta;

        if (wave < waveDataList.size()) {
            if (time >= waveDataList.get(wave).time) {
                do {
                    gameContainer.addEnemy(waveDataList.get(wave).enemy);
                    wave++;
                } while (wave < waveDataList.size() && waveDataList.get(wave - 1).time == waveDataList.get(wave).time);
            }
        }
    }

    public Enemy stringToEnemy(String enemy, float x, float y) {
        EnemyData enemyData = enemyList.getEnemyData(enemy);
        return new Enemy(x, y, enemyData.movement.x, enemyData.movement.y, enemyData.hitPoint, gameContainer, enemyData.texture, enemyData.hitBox);
    }
}
