package com.entek.boghu_shooter;

import com.badlogic.gdx.math.Rectangle;

public class CollisionController {
    private GameContainer gameContainer;

    CollisionController(GameContainer gameContainer) {
        this.gameContainer = gameContainer;
    }

    public void update(float delta) {
        for (Bullet bullet : gameContainer.getBulletList()) {
            for (Enemy enemy : gameContainer.getEnemyList()) {
                checkCollision(bullet, enemy);
            }
        }
    }

    public void checkCollision(Bullet bullet, Enemy enemy) {
        for (Rectangle r1 : bullet.getHitBox().getHitBoxList()) {
            for (Rectangle r2 : enemy.getHitBox().getHitBoxList()) {
                if(bullet.getPosition().x + r1.x < enemy.getPosition().x + r2.x + r2.width &&
                        bullet.getPosition().x + r1.x + r1.width > enemy.getPosition().x + r2.x &&
                        bullet.getPosition().y + r1.y < enemy.getPosition().y + r2.y + r2.height &&
                        bullet.getPosition().y + r1.y + r1.height > enemy.getPosition().y + r2.y) {
                    bullet.collision(enemy);
                    enemy.collision(bullet);
                }
            }
        }
    }
}
