package com.entek.boghu_shooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class GameRenderer {
    private GameWorld gameWorld;
    private TextureManager textureManager = new TextureManager("textures.csv", "animation.csv");
    private Batch batch = new SpriteBatch();
    private OrthographicCamera cam = new OrthographicCamera();
    private ShapeRenderer shapeRenderer = new ShapeRenderer();
    private float stateTime = 0;
    private int addedHeight = 40;

    public GameRenderer(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        cam.setToOrtho(false, 256, 240);
        batch.setProjectionMatrix(cam.combined);
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stateTime += delta;

        Background[] background_lvl1 = textureManager.getBackgroundLevel_1();

        batch.begin();
        for (Background background : background_lvl1) {
            background.update(Gdx.graphics.getDeltaTime());
            batch.draw(background.getTexture(), background.getX1(), 0);
            batch.draw(background.getTexture(), background.getX2(), 0);
        }

        for (Bullet bullet : gameWorld.getGameContainer().getBulletList()) {
            batch.draw(textureManager.getTexture("bullet"), bullet.getPosition().x, bullet.getPosition().y);
        }

        for (Enemy enemy : gameWorld.getGameContainer().getEnemyList()) {
            batch.draw(textureManager.getTexture(enemy.getTexture()), enemy.getPosition().x, enemy.getPosition().y);
        }

        batch.draw(textureManager.getTexture("plane"), gameWorld.getPlane().getPosition().x, gameWorld.getPlane().getPosition().y);

        for (Particle particle : gameWorld.getGameContainer().getParticleList()) {
            TextureRegion currFrame;
            if (particle instanceof Explosion) {
                currFrame = textureManager.getAnimation("explosion").getKeyFrame(particle.getDuration(), false);
            } else if (particle instanceof EnemyDestroyed){
                currFrame = textureManager.getAnimation("enemyDestroyed").getKeyFrame(particle.getDuration(), true);
            } else {
                currFrame = textureManager.getAnimation("enemyExplosion").getKeyFrame(particle.getDuration(), false);
            }
            batch.draw(currFrame, particle.getPosition().x - particle.getWidth() / 2.0f, particle.getPosition().y - particle.getHeight() / 2.0f);
        }

        batch.end();
    }

    public void dispose() {
        textureManager.dispose();
    }

    public OrthographicCamera getCam() { return cam; }
}
