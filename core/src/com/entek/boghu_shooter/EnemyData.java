package com.entek.boghu_shooter;

import com.badlogic.gdx.math.Vector2;

public class EnemyData {
    public String texture;
    public Vector2 movement;
    public float hitPoint;
    public HitBox hitBox;

    EnemyData(String texture, float x, float y, float hitPoint, HitBox hitBox) {
        this.texture = texture;
        this.movement = new Vector2(x, y);
        this.hitPoint = hitPoint;
        this.hitBox = hitBox;
    }
}
