package com.entek.boghu_shooter;

public class WaveData {
    public Enemy enemy;
    public float time;

    WaveData(Enemy enemy, float time) {
        this.enemy = enemy;
        this.time = time;
    }
}
