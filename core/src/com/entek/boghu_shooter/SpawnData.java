package com.entek.boghu_shooter;

import com.badlogic.gdx.math.Vector2;

public class SpawnData {
    public String enemyName;
    public Vector2 position;
    public float time;

    SpawnData(String enemyName, Vector2 position, float time) {
        this.enemyName = enemyName;
        this.position = position;
        this.time = time;
    }
}
