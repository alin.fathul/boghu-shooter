package com.entek.boghu_shooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

public abstract class Entity {
    private Vector2 position;
    private Vector2 velocity;
    private float width;
    private float height;
    private HitBox hitBox = new HitBox();

    Entity(Vector2 position, Vector2 velocity, float width, float height) {
        this.position = position;
        this.velocity = velocity;
        this.width = width;
        this.height = height;
    }

    public abstract void update(float delta);

    public abstract void collision(Entity entity);

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public void addPosition(float x, float y) {
        this.position.x += x;
        this.position.y += y;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }

    public HitBox getHitBox() { return hitBox; }

    public void setHitBox(HitBox hitBox) {
        this.hitBox = hitBox;
    }

    public float getHeight() {
        return height;
    }

    public float getWidth() {
        return width;
    }
}
