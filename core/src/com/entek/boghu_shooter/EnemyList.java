package com.entek.boghu_shooter;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashMap;

public class EnemyList {
    private HashMap<String, EnemyData> enemyDataHashMap = new HashMap<>();

    public void add(String name, float x, float y, float hitPoint, String texture, HitBox hitBox) {
        enemyDataHashMap.put(name, new EnemyData(texture, x, y, hitPoint, hitBox));
    }

    public EnemyData getEnemyData(String name) {
        return enemyDataHashMap.get(name);
    }
}
