package com.entek.boghu_shooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Scanner;

public class SpawnPattern {
    private FileHandle file;
    private Scanner scanner;
    private ArrayList<SpawnData> spawnDataList = new ArrayList<>();

    SpawnPattern(String fileName) {
        this.file = Gdx.files.internal(fileName);
        this.scanner = new Scanner(file.read());

        while (scanner.hasNextLine()) {
            float time = Float.parseFloat(scanner.nextLine());
            String enemy = scanner.nextLine();
            String[] position = scanner.nextLine().split(" ");

            float x = Float.parseFloat(position[0]);
            float y = Float.parseFloat(position[1]);

            SpawnData spawnData = new SpawnData(enemy, new Vector2(x, y), time);
            spawnDataList.add(spawnData);
        }
    }

    public ArrayList<SpawnData> getSpawnDataList() {
        return spawnDataList;
    }
}
