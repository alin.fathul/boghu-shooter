package com.entek.boghu_shooter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.math.Vector2;

public abstract class Particle {
    private float duration;
    private Vector2 position;
    private GameContainer gameContainer;
    private int width;
    private int height;
    private float durationTimer = 0;

    Particle(float duration, float x, float y, GameContainer gameContainer, int width, int height) {
        this.duration = duration;
        this.position = new Vector2(x, y);
        this.gameContainer = gameContainer;
        this.width = width;
        this.height = height;
    }

    public abstract void update(float delta);

    public void updateTimer(float delta) {
        durationTimer += delta;
        if(durationTimer >= duration) {
            durationTimer -= duration;
            gameContainer.removeParticle(this);
        }
    }

    public float getDuration() {
        return durationTimer;
    }

    public Vector2 getPosition() {
        return position;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void addPosition(float x, float y) {
        position.x += x;
        position.y += y;
    }
}
