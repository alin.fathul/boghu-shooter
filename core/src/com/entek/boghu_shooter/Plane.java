package com.entek.boghu_shooter;

import com.badlogic.gdx.math.Vector2;

public class Plane extends Entity {
    private float speed;
    private float mass;
    private float maxForce;
    private float slowingRadius;
    private float fireRate;
    private Vector2 planeSize;
    private Vector2 targetPosition = new Vector2(0, 0);
    private GameContainer gameContainer;
    private float spawnTimer = 0;
    private Vector2 bulletSpawnPosition;

    Plane(float maxForce, float mass, float speed, float slowingRadius, float fireRate, int planeSizeX, int planeSizeY, float startPosX, float startPosY, GameContainer gameContainer, float bulletSpawnX, float bulletSpawnY) {
        super(new Vector2(startPosX, startPosY), new Vector2(0, 0), 48, 26);

        this.maxForce = maxForce;
        this.mass = mass;
        this.speed = speed;
        this.slowingRadius = slowingRadius;
        this.planeSize = new Vector2(planeSizeX, planeSizeY);
        this.gameContainer = gameContainer;
        this.bulletSpawnPosition = new Vector2(bulletSpawnX, bulletSpawnY);
        this.fireRate = fireRate;
    }

    @Override
    public void update(float delta) {
        spawnTimer += delta;
        if(spawnTimer >= fireRate){
            spawnTimer -= fireRate;
            shoot();
        }

        moveToTarget();
        truncate(super.getVelocity(), speed);
        super.addPosition(getVelocity().x * delta, getVelocity().y * delta);
    }

    @Override
    public void collision(Entity entity) {

    }

    public void setTargetPosition(float x, float y) {
        this.targetPosition.x = x - getWidth() / 2.0f;
        this.targetPosition.y = y - getHeight() / 2.0f;
    }

    public void moveToTarget() {
        Vector2 steering = new Vector2(targetPosition.x - getPosition().x, targetPosition.y - getPosition().y);
        float distance = Vector2.dst(getPosition().x, getPosition().y, targetPosition.x, targetPosition.y);
        if (distance < slowingRadius) {
            steering.nor();
            steering.scl(speed * distance / slowingRadius);
        } else {
            steering.nor();
            steering.scl(speed);
        }

        steering.sub(getVelocity());
        truncate(steering, maxForce);
        steering.scl(1 / mass);

        getVelocity().add(steering);
    }

    public void shoot() {
        Bullet bullet = new Bullet(500, getPosition().x + bulletSpawnPosition.x, getPosition().y + bulletSpawnPosition.y + 2.5f, gameContainer, 1);
        Bullet bullet2 = new Bullet(500, getPosition().x + bulletSpawnPosition.x, getPosition().y + bulletSpawnPosition.y - 2.5f, gameContainer, 1);
        gameContainer.addBullet(bullet);
        gameContainer.addBullet(bullet2);
    }

    public void truncate(Vector2 vector2, float max) {
        float i;
        i = max / (float)Math.sqrt(Math.pow(vector2.x, 2) + Math.pow(vector2.y, 2));
        i = Math.min(i, 1.0f);
        vector2.scl(i);
    }

    public void addTargetPositionY(float y) {
        this.targetPosition.y += y;
    }
}
